#!/usr/bin/env bash

rm -rf auto_fig_py*.png *.aux *.bbl *.bcf *.blg *.log _minted-slides *.nav *.out *.pgf pythontex-files-* *.pytxcode *.run.xml *.snm *.toc *.vrb
